﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Calculo_Mental.Startup))]
namespace Calculo_Mental
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
